use std::io;

fn main() {
    let mut input = String::new();
    println!("Please enter the numbers you want to sort, seperated by a space");
    io::stdin().read_line(&mut input).expect("Failed to read line");
    let mut numbers: Vec<i32> = input.split_whitespace().map(|s| s.parse().unwrap()).collect();
    println!("Please enter the number of the sorting method you want to use");
    println!("1: Bubble Sort");
    println!("2: Selection Sort");
    println!("3: Insertion Sort");
    println!("4: Quick Sort");
    println!("5: Merge Sort");
    let mut method = String::new();
    io::stdin().read_line(&mut method).expect("Failed to read line");
    let method: i32 = method.trim().parse().expect("Please type a number!");
    match method {
        1 => bubble_sort(&mut numbers),
        2 => selection_sort(&mut numbers),
        3 => insertion_sort(&mut numbers),
        4 => quick_sort(&mut numbers),
        5 => merge_sort(&mut numbers),
        _ => println!("Please enter a number between 1 and 5"),
    }
    println!("{:?}", numbers);
}

fn bubble_sort(numbers: &mut Vec<i32>) {
    let mut swapped = true;
    while swapped {
        swapped = false;
        for i in 0..numbers.len() - 1 {
            if numbers[i] > numbers[i + 1] {
                numbers.swap(i, i + 1);
                swapped = true;
            }
        }
    }
}

fn selection_sort(numbers: &mut Vec<i32>) {
    for i in 0..numbers.len() {
        let mut min = i;
        for j in i + 1..numbers.len() {
            if numbers[j] < numbers[min] {
                min = j;
            }
        }
        numbers.swap(i, min);
    }
}

fn insertion_sort(numbers: &mut Vec<i32>) {
    for i in 1..numbers.len() {
        let mut j = i;
        while j > 0 && numbers[j - 1] > numbers[j] {
            numbers.swap(j - 1, j);
            j -= 1;
        }
    }
}

fn quick_sort(numbers: &mut Vec<i32>) {
    quick_sort_helper(numbers, 0, numbers.len() - 1);
}

fn quick_sort_helper(numbers: &mut Vec<i32>, low: usize, high: usize) {
    if low < high {
        let p = partition(numbers, low, high);
        quick_sort_helper(numbers, low, p - 1);
        quick_sort_helper(numbers, p + 1, high);
    }
}

fn partition(numbers: &mut Vec<i32>, low: usize, high: usize) -> usize {
    let pivot = numbers[high];
    let mut i = low;
    for j in low..high {
        if numbers[j] <= pivot {
            numbers.swap(i, j);
            i += 1;
        }
    }
    numbers.swap(i, high);
    i
}

fn merge_sort(numbers: &mut Vec<i32>) {
    let mut temp = vec![0; numbers.len()];
    merge_sort_helper(numbers, &mut temp, 0, numbers.len() - 1);
}

fn merge_sort_helper(numbers: &mut Vec<i32>, temp: &mut Vec<i32>, low: usize, high: usize) {
    if low < high {
        let mid = (low + high) / 2;
        merge_sort_helper(numbers, temp, low, mid);
        merge_sort_helper(numbers, temp, mid + 1, high);
        merge(numbers, temp, low, mid, high);
    }
}

fn merge(numbers: &mut Vec<i32>, temp: &mut Vec<i32>, low: usize, mid: usize, high: usize) {
    for i in low..high + 1 {
        temp[i] = numbers[i];
    }
    let mut i = low;
    let mut j = mid + 1;
    let mut k = low;
    while i <= mid && j <= high {
        if temp[i] <= temp[j] {
            numbers[k] = temp[i];
            i += 1;
        } else {
            numbers[k] = temp[j];
            j += 1;
        }
        k += 1;
    }
    while i <= mid {
        numbers[k] = temp[i];
        k += 1;
        i += 1;
    }
}